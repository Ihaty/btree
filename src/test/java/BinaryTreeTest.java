import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BinaryTreeTest {
    private static BsTree tree;

            @BeforeAll
    public static void setUp() {
                tree = new BinaryTree();
            }

            @BeforeEach
    public void clearTree() {
                tree.clear();
            }

            @Test
    public void test_print1() {
                int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
                tree.init(array);
                tree.print();
            }

            @Test
    public void test_add_1() {
                int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
                int[] expected = new int[]{-20, -15, -12, -10, 0, 5, 6, 20, 25, 28, 35};
                tree.init(array);
                tree.add(28);
                tree.add(-12);
                tree.add(-20);

                int[] actual = tree.toArray();
                assertArrayEquals(expected, actual);
            }

            @Test
    public void test_size_1() {
                int exp = 8;
                int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
                tree.init(array);
                int act = tree.size();
                assertEquals(exp, act);
            }

            @Test
    public void testDelete(){
                int[] array = new int[]{5, -15, 20, 35, 0, 6, -10, 25};
                tree.init(array);
                int[] exp = new int[]{ -15, -10, 0, 6, 20, 25, 35};
                tree.del(5);
                int[] act = tree.toArray();
                assertArrayEquals(exp, act);
            }

            @Test
    public void testNodes(){
                int exp = 7;
                int[] array = new int[]{6,8,13,-4,-1,5,20,12,11,14,22};
                tree.init(array);
                int act = tree.nodes();
                assertEquals(exp, act);
            }

            @Test
    public void testLeaves(){
                int exp = 4;
                int[] array = new int[]{6,8,13,-4,-1,5,20,12,11,14,22};
                tree.init(array);
                int act = tree.leaves();
                assertEquals(exp, act);

                   }

            @Test
    public void testGetWidth(){
                int[] array = new int[]{6,8,13,-4,-1,5,20,12,11,14,22};
                tree.init(array);
                int exp = 4;
                int act = tree.getWidth();
                assertEquals(exp, act);
            }

            @Test
    public void testGetHeight(){
                int exp = 5;
                int[] array = new int[]{6,8,13,-4,-1,5,20,12,11,14,22};
                tree.init(array);
                int act = tree.getHeight();
                assertEquals(exp, act);
            }

            @Test
    public void testReverse(){
                int exp = 5;
                int[] array = new int[]{6,8,13,-4,-1,5,20,12,11,14,22};
                tree.init(array);
                tree.reverse();
                int[] act = tree.toArray();
                for (int i : act){
                        System.out.println(i);
                    }
            }
}
