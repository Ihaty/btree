public interface BsTree {
    void print();

    void init(int[] ar);
    void clear();
    int size();
    int[] toArray();

    void add(int val);
    void del(int val);

    int getWidth();
    int getHeight();

    int nodes();
    int leaves();

    void reverse();
}
