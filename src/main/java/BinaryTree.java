
public class BinaryTree implements BsTree {
    private Node root = null;

    @Override
    public void print() {
        printNode(this.root);
    }

    private void printNode(Node node) {
        if (node == null) {
            return;
        }
        printNode(node.left);
        System.out.print(node.value + ", ");
        printNode(node.right);
    }

    @Override
    public void init(int[] ar) {
        if (ar == null) {
            ar = new int[0];
        }
        for (int value : ar) {
            add(value);
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        Counter counter = new Counter();
        return nodeSize(this.root, counter);
    }

    private int nodeSize(Node node, Counter counter) {
        if (node == null) {
            return 0;
        }
        nodeSize(node.left, counter);
        counter.index++;
        nodeSize(node.right, counter);
        return counter.index;
    }

    @Override
    public int[] toArray() {
        int[] ar = new int[size()];
        nodeToArray(this.root, ar, new Counter());
        return ar;
    }

    private void nodeToArray(Node node, int[] ar, Counter counter) {
        if (node == null) {
            return;
        }
        nodeToArray(node.left, ar, counter);
        ar[counter.index++] = node.value;
        nodeToArray(node.right, ar, counter);
    }

    @Override
    public void add(int val) {
        if (this.root == null) {
            this.root = new Node(val);
            return;
        }
        addNode(this.root, val);
    }

    private void addNode(Node node, int value) {
        if (value < node.value) {
            if (node.left == null) {
                node.left = new Node(value);
            } else {
                addNode(node.left, value);
            }
        } else {
            if (node.right == null) {
                node.right = new Node(value);
            } else {
                addNode(node.right, value);
            }
        }
    }

    @Override
    public void del(int val) {
        if (this.root == null) {
            System.out.println("root is empty");
        } else {
            int[] arr = toArray();
            this.root = null;
            for (int i : arr) {
                if (val == i) {
                    continue;
                }
                add(i);
            }
        }
    }

    @Override
    public int getWidth() {
        return leaves();
    }

    @Override
    public int getHeight() {
        return heightOfBinaryTree(this.root);
    }

    public int heightOfBinaryTree(Node node) {
        if (node == null) {
            return 0;
        } else {
            return 1 + Math.max(heightOfBinaryTree(node.left), heightOfBinaryTree(node.right));
        }
    }

    @Override
    public int nodes() {
        if (this.root == null) {
            return 0;
        }
        return size() - leaves();
    }


    @Override
    public int leaves() {
        return leavesCount(this.root, new Counter());
    }

    private int leavesCount(Node node, Counter counter) {
        if (node == null) {
            return counter.index;
        }
        leavesCount(node.left, counter);
        if (node.left == null && node.right == null) {
            ++counter.index;
        }
        leavesCount(node.right, counter);
        return counter.index;
    }

    @Override
    public void reverse() {
        Node temp = root.right;
        root.right = root.left;
        root.left = temp;
    }

    private class Node {
        int value;
        Node left;
        Node right;

        public Node(int value) {
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }

    private class Counter {
        int index = 0;
    }
}
